import socket
import os
import hashlib
from multiprocessing import Pool
import math
import time
import numpy as np # for generating list of integers
import pandas as pd
import matplotlib.pyplot as plt
#%matplotlib inline

def hash_fact(nums):
    for num in nums:
        fact = math.factorial(num)
        m = hashlib.md5(str(fact).encode()).digest()
    return m

def forkwork(nums, num_of_workers):
    pool = Pool(processes=num_of_workers)
    groups = [nums[i::num_of_workers] for i in range(num_of_workers+1)]
    result = pool.map(hash_fact, groups)
    return result

def main_server_function(port: int = 8000, num_of_workers: int = 2):
    '''
    :param port : port number to accept the incoming requests
    :param num_of_workers : number of workers to handle the requests
    '''
    host = '127.0.0.1'
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(5)
    while 1:
        connection, addr = server_socket.accept()
        pid = os.fork()
        if pid == 0:
            with connection:
                while 1:
                    data = connection.recv(1024).decode()
                    if not data:
                        break
                    if data:
                        result = forkwork(data, num_of_workers)
                        connection.send(result.encode())
                        break
        else:
            continue
            

def check_effectiveness():
    data = np.random.randint(1, high=100, size=1000000).tolist()
    d = {}
    # i is a number of workers
    for i in range(2,6):
        start = time.time()
        forkwork(data, i)
        end = time.time()
        d[f'{i} threads'] = end-start
    start1 = time.time()
    hash_fact(data)
    end1 = time.time()
    d['1 thread'] = end1-start1
    res = pd.DataFrame(d, index=(0,))
    print(res)
    plt.plot(range(1,6), list(d.values()),   'r', marker='o')
    plt.grid()
    plt.xticks(range(1,6))
    plt.xlabel('Threads')
    plt.ylabel('Time')
    plt.show()
    
check_effectiveness()