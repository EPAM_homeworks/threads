import threading
import time
import sys

sem = threading.Semaphore()


def fun1():
    while True:
        sem.acquire()
        print(1)
        sem.release()
        time.sleep(0.25)

def fun2():
    while True:
        sem.acquire()
        print(2)
        sem.release()
        time.sleep(0.25)

def threads():
    t1 = threading.Thread(target = fun1)
    t1.start()
    t2 = threading.Thread(target = fun2)
    t2.start()

try:
   threads()
except KeyboardInterrupt:
    pass
finally:
    sys.exit()