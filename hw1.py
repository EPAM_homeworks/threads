import psutil
import getpass
import pwd
import os


def process_count(username: str) -> int:
    # к-во процессов, запущенных из-под текущего пользователя
    UID   = 1
    EUID  = 2

    def owner(pid):
        '''Return username of UID of process pid'''
        for ln in open('/proc/%d/status' % pid):
            if ln.startswith('Uid:'):
                uid = int(ln.split()[UID])
                return pwd.getpwuid(uid).pw_name
            
    pids = [int(pid) for pid in os.listdir('/proc') if pid.isdigit() if owner(int(pid))==username]
    return len(pids)      


def total_memory_usage(root_pid: int) -> int:
    # суммарное потребление памяти древа процессов
    process = psutil.Process(root_pid)
    papa_memory = process.memory_info().rss
    mem_list = [child.memory_info().rss for child in papa_memory.children()+papa_memory]
    return sum(mem_list)

#name = getpass.getuser()
#process_count(name)